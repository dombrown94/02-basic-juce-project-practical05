//
//  ThomasSangerButton.h
//  JuceBasicWindow
//
//  Created by Dominic Brown on 10/30/14.
//
//

#ifndef __JuceBasicWindow__ThomasSangerButton__
#define __JuceBasicWindow__ThomasSangerButton__

#include "../JuceLibraryCode/JuceHeader.h"

class ThomasSangerButton: public Component
{
public:
    ThomasSangerButton();
    ~ThomasSangerButton();
    
    void paint(Graphics& g) override;
    
    void mouseEnter (const MouseEvent& event) override;
    void mouseDown (const MouseEvent& event) override;
    void mouseUp (const MouseEvent& event) override;
    void mouseExit (const MouseEvent& event) override;
    
private:
    
};



#endif /* defined(__JuceBasicWindow__ThomasSangerButton__) */
