//
//  ThomasSangerButton.cpp
//  JuceBasicWindow
//
//  Created by Dominic Brown on 10/30/14.
//
//

#include "ThomasSangerButton.h"


ThomasSangerButton::ThomasSangerButton()
{

}
ThomasSangerButton::~ThomasSangerButton()
{
    
}

void ThomasSangerButton::paint(Graphics& g)
{
    g.setColour(Colours::red);
    g.fillRect(0,0,getWidth()/2,getHeight()/2);
}
void ThomasSangerButton::mouseEnter (const MouseEvent& event)
{
    
}
void ThomasSangerButton::mouseDown (const MouseEvent& event)
{
    
}
void ThomasSangerButton::mouseUp (const MouseEvent& event)
{
    repaint();
}
void ThomasSangerButton::mouseExit (const MouseEvent& event)
{
    
}