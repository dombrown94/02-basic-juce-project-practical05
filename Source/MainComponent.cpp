/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"



//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    
//    textButton1.setButtonText("Click Me m8");
//    addAndMakeVisible(&textButton1);
//    textButton1.addListener(this);
//    
//    textButton2.setButtonText("No Click Me m8");
//    addAndMakeVisible(&textButton2);
//    textButton2.addListener(this);
//    
//    slider1.setSliderStyle(Slider::LinearBar);
//    addAndMakeVisible(&slider1);
//    slider1.addListener(this);
//    
//    comboBox1.addItem("U Wot m8", 1);
//    comboBox1.addItem("I'll rek ya m8", 2);
//    comboBox1.addItem("I swear on me mam", 3);
//    addAndMakeVisible(comboBox1);
//    
//    textEditor1.setText("U fekin say nowt or Ill bash ur hed in");
//    addAndMakeVisible(textEditor1);

//    addAndMakeVisible(&colour1);
    
    addAndMakeVisible(&tsb);

}

MainComponent::~MainComponent()
{
    
}

void MainComponent::resized()
{
    DBG("resized\twidth\t" << getWidth() << "\theight\t" << getHeight() << "\n");
    
//    textButton1.setBounds(10, 10, getWidth() - 20, 40);
//    textButton2.setBounds(10, 50, getWidth() -20, 40);
//
//    slider1.setBounds(10, 90, getWidth()-20, 40);
//    comboBox1.setBounds(10, 130, getWidth()-20, 40);
//    
//    textEditor1.setBounds(10, 170, getWidth() - 20, 40);
    
    colour1.setBounds(10, 10, getWidth()/2.0, getHeight()/2.0);
    
    tsb.setBounds (getBounds());
}

void MainComponent::buttonClicked(Button* button)
{
    if (button == &textButton1) {
        DBG("Button1 Clicked\n");
    }
    else if (button == &textButton2){
        DBG("Button2 Clicked");
    }
}

void MainComponent::sliderValueChanged(Slider* slider)
{
    if (slider == &slider1) {
        DBG("slider1 value changed");
        DBG("slider1 value:" << slider->getValue());
    }
}

void MainComponent::paint(Graphics& g)
{
    std::cout << "Paint is being called\n";
    
    g.setColour(colour1.getCurrentColour());
    //g.drawEllipse(x-5, y-5, 10, 10, 1);
    g.fillEllipse(x-5, y-5, 10, 10);
}


void MainComponent::mouseUp (const MouseEvent& event)
{
    x = event.x;
    y = event.y;
    std::cout << "x: " << x << " y: " << y << "\n";
    repaint();
}